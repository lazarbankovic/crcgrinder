#include <iostream>
#include <string>
#include "dataparser.h"
#include "args.hxx"
#include "crcchrcker.h"
#include "stdint.h"
#include <iomanip>

using namespace std;

inline void checkIt(const DataParser& dataParser, CrcChecker& checker) {
    bool pass = true;
    dataParser.forAll([&](const DataParser::Metadata &md){
        pass = pass && checker.checkCrc(md.data, md.expectedValue.val);
    });
    if(pass)
       cout << "Polynom: "  << std::uppercase << std::setfill('0') << std::setw(2) << hex << checker.polynom
            << " crcinit: " << std::setfill('0') << std::setw(2) << hex << checker.crcinit
            << " crcxor: " << std::setfill('0') << std::setw(2) << hex << checker.crcxor
            << " refin: " << checker.refin
            << " refout: " << checker.refout
            << " direction: " << checker.direct
            << endl;
}

int main(int argc, char **argv) {
    args::ArgumentParser args("Crc grinder");
    args::HelpFlag help(args, "help", "Display this help menu", {'h', "help"});
    args::ValueFlag<std::string> polynom(args, "hex", "Crc polynom, omit it and it will grind all values", {'p', "polynom"});
    args::ValueFlag<std::string> order(args, "dec", "Crc order, [8, 16, 32]", {'o', "order"});
    args::ValueFlag<std::string> refin(args, "dec", "[0,1] specifies if a data byte is reflected before processing or not", {'i', "refin"});
    args::ValueFlag<std::string> refout(args, "dec", "[0,1] specifies if the CRC will be reflected before XOR", {'r', "refout"});
    args::ValueFlag<std::string> direct(args, "dec", "[0,1] specifies the kind of algorithm: 1=direct, no augmented zero bits", {'d', "direct"});
    args::Positional<std::string> file(args, "file", "Input data file");

    try {
        args.ParseCLI(argc, argv);
    } catch (args::Help) {
        std::cout << args;
        return EXIT_SUCCESS;
    } catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << args;
        return EXIT_FAILURE;
    } catch (args::ValidationError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << args;
        return EXIT_FAILURE;
    }

    if (!file)
        std::cerr << "No input file" << std::endl;

    int crcOrder = 8; //default
    if (order) {
        crcOrder = std::stoul(args::get(order), nullptr, 10);
        if(crcOrder != 8 && crcOrder != 16 && crcOrder !=32) {
            cerr << "order must be 8, 16 or 32" << endl;
            return EXIT_FAILURE;
        }
    }

    unsigned long polynomMin = 0x00, polynomMax = 0xff, polyParsed = 0;
    if(crcOrder == 16) polynomMax = 0xffFF;
    if(crcOrder == 32) polynomMax = 0xffFFffFF;
    if (polynom) {
        polyParsed = std::stoul(args::get(polynom), nullptr,  16);
        polynomMax=polynomMin=polyParsed;
        cout << "Polynom 0x" << hex << polyParsed << endl;
    }

    int refinMin = 0, refinMax = 1, refinParsed;
    if(refin) {
        refinParsed = std::stoul(args::get(refin), nullptr, 10);
        refinMin = refinMax = refinParsed;
        cout << "Refin: " << dec << refinParsed << endl;
    }

    int refoutMin = 0, refoutMax = 1, refoutParsed;
    if(refout) {
        refoutParsed = std::stoul(args::get(refout), nullptr, 10);
        refoutMin = refoutMax = refoutParsed;
        cout << "Refout: " << dec << refoutParsed << endl;
    }

    int directionMin = 0, directionMax = 1, directionParsed;
    if(direct) {
        directionParsed = std::stoul(args::get(direct), nullptr, 10);
        directionMin = directionMax = directionParsed;
        cout << "Direction: " << dec << directionParsed << endl;
    }

    try {

        DataParser dataParser(args::get(file), (DataParser::Order)crcOrder);
        cout << "Parsed data:" << endl << dataParser << endl;
        cout << "Frames: " << dataParser.size() << endl;
        CrcChecker checker;
        checker.order = crcOrder;

        for( checker.polynom=polynomMin; checker.polynom <= polynomMax; checker.polynom++) {
            cout << "Polynom " << hex << "0x"<<(int)checker.polynom << "/0x" << polynomMax << endl;
            for( checker.refin = refinMin; checker.refin <= refinMax ; checker.refin++)
                for( checker.direct = directionMin; checker.direct <= directionMax ; checker.direct++)
                    for( checker.refout = refoutMin; checker.refout <= refoutMax ; checker.refout++)
                        for( checker.crcinit = 0x00; checker.crcinit <= 0xff; checker.crcinit++)
                            for( checker.crcxor = 0x00; checker.crcxor <= 0xff; checker.crcxor++)
                                checkIt(dataParser, checker);
        }

    } catch (const std::exception &e) {
        cerr << e.what() << endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
