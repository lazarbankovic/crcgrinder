#ifndef GRINDER_H
#define GRINDER_H

#include <vector>

class CrcChecker {
public:
    CrcChecker();
    bool checkCrc(const std::vector<unsigned char> &data, unsigned long expectedValue);

    int order = 8; // CRC-8
    unsigned long polynom = 0;
    int direct = 1;
    unsigned long crcinit = 0;
    unsigned long crcxor = 0;
    int refin = 0;
    int refout = 0;

private:


    unsigned long crcmask;
    unsigned long crchighbit;
    unsigned long crcinit_direct;
    unsigned long crcinit_nondirect;
    unsigned long crctab[256];

    unsigned long reflect (unsigned long crc, int bitnum);
    void generate_crc_table();
    unsigned long crctablefast(unsigned char *p, unsigned long len);
    unsigned long crctable(unsigned char *p, unsigned long len);
    unsigned long crcbitbybit(unsigned char *p, unsigned long len);
    unsigned long crcbitbybitfast(unsigned char *p, unsigned long len);
};

#endif

