#ifndef DATAPARSER_H
#define DATAPARSER_H

#include <string>
#include <vector>
#include <functional>
#include <ostream>

class DataParser
{
public:
    enum Order{ORDER8=8, ORDER16=16, ORDER32=32};
    struct Metadata {
        union ExpectedData{
            uint8_t order[3];
            unsigned long val;
        } expectedValue;
        std::vector<uint8_t> data;
    };

    DataParser(const std::string &fileName, Order o = ORDER8);
    size_t size() const;
    void forAll(const std::function<void (const DataParser::Metadata &)> &f) const;
    friend std::ostream& operator<<(std::ostream &os, const DataParser &dp);
private:
    std::vector<Metadata> data;
    std::string fileName;
    void parse(Order order);
    std::vector<uint8_t> hexToBytes(const std::string &hex);

};

#endif // DATAPARSER_H
