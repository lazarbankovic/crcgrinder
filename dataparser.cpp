#include "dataparser.h"
#include <sstream>
#include <string>
#include <fstream>
#include <algorithm>
#include <regex>

#include <iostream>
#include <iomanip>

DataParser::DataParser(const std::string &fileName, Order order): fileName(fileName) {
    parse(order);
}

size_t DataParser::size() const {
    return data.size();
}

void DataParser::forAll(const std::function<void (const DataParser::Metadata &)> &f) const{
    for(const auto &meta: data)
        f(meta);
}

std::ostream & operator<<(std::ostream &os, const DataParser &dp) {
    dp.forAll([&](const DataParser::Metadata &metadata){
        for(const auto &byte: metadata.data)
            os << std::uppercase << std::setfill('0') << std::setw(2) << std::hex
               << static_cast<int>(byte) << " ";
        os << " , expectedValue: 0x"
           << std::setfill('0') << std::hex
           << static_cast<int>(metadata.expectedValue.val)
           << std::endl;
    });
    return os;
}


void DataParser::parse(Order order)
{
    std::ifstream infile(fileName);

    if(!infile.is_open())
        throw std::runtime_error("Error opening file: " + fileName);

    std::string line;

    while (std::getline(infile, line)) {
        line = std::regex_replace(line, std::regex("0x"), "");
        line = std::regex_replace(line, std::regex("[^A-F|^a-f|^0-9]"), ""); // All but the actual hex values

        if(line.empty())
            continue;
        if(line[0]=='#')
            continue;

        auto temp = hexToBytes(line);
        Metadata::ExpectedData expected;
        switch (order) {
        case ORDER8:
            expected.val = temp.back();
            temp.pop_back();
            break;
        case ORDER16:
            expected.val = 0;
            expected.order[0] = temp.back();
            temp.pop_back();
            expected.order[1] = temp.back();
            temp.pop_back();
            break;
        case ORDER32:
            expected.val = 0;
            expected.order[0] = temp.back();
            temp.pop_back();
            expected.order[1] = temp.back();
            temp.pop_back();
            expected.order[2] = temp.back();
            temp.pop_back();
            break;
        default:
            expected.val = temp.back();
            temp.pop_back();
        }

        data.push_back({ expected, temp });
    }
}

std::vector<uint8_t> DataParser::hexToBytes(const std::string& hex) {
  std::vector<uint8_t> bytes;

  for (unsigned int i = 0; i < hex.length(); i += 2) {
    std::string byteString = hex.substr(i, 2);
    uint8_t byte = (uint8_t) strtol(byteString.c_str(), nullptr, 16);
    bytes.push_back(byte);
  }

  return bytes;
}
